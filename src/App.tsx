import { useState, useEffect } from "react";
import axios from "axios";

import "./App.css";

const baseUrl = "http://192.168.0.108:4000/files";

function App() {
  const [filesData, setFilesData] = useState([]);
  const [currentPath, setCurrentPath] = useState("");

  useEffect(() => {
    getDirectory("");
  }, []);

  function getDirectory(name: string) {
    axios
      .get(
        `${baseUrl}/directory${
          name?.length ? "/" + name : ""
        }?currentPath=${currentPath}`,
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(({ data }) => {
        console.log(data);
        setFilesData(data);
      });
  }

  function deleteFile(filename: string) {
    axios
      .delete(`${baseUrl}/${filename}?currentPath=${currentPath}`)
      .then(({ data }) => {
        console.log(data);
        getDirectory(currentPath);
      })
      .catch(err => {
        console.log(err);
      })
  }

  return (
    <>
      <button
        onClick={() => {
          getDirectory("");
          setCurrentPath("");
        }}
        style={{
          color: "white",
          backgroundColor: "teal",
        }}
      >
        Home
      </button>
      <br />
      Upload files
      <input
        style={{
          margin: "4px 4px",
        }}
        type="file"
        multiple={true}
        onChange={async (ev) => {
          const formData = new FormData();

          const files = ev.target.files?.length ? [...ev.target.files] : [];
          console.log(files);

          files.forEach((file) => {
            formData.append("files", file);
            console.log(file);

            console.log(formData);
          });

          // formData.append('file', files[0]);

          try {
            const { data } = await axios.post(
              `${baseUrl}/uploadFiles`,
              formData,
              {
                headers: {
                  "Content-Type": "application/form-data; charset=utf-8",
                },
              }
            );

            getDirectory("");
            console.log(data);
          } catch (error) {
            console.error(error);
          }
        }}
      />
      <ul>
        {filesData.map(
          (item: { filename: string; isDirectory: boolean }, index) => (
            <li
              key={index}
              onClick={(ev) => {
                if (ev.target.localName === 'button') {
                  return;
                }
                
                if (item.isDirectory) {
                  axios
                    .get(
                      `${baseUrl}/directory/${item.filename}${
                        currentPath ? "currentPath=" + currentPath : ""
                      }`
                    )
                    .then(({ data }) => {
                      setFilesData(data);
                      setCurrentPath(currentPath + "/" + item.filename);
                    });
                } else {
                  const anchorElement = document.createElement("a");

                  console.log("item.filename -------------");
                  console.log(item.filename);

                  anchorElement.href = `${baseUrl}/download/${item.filename}${
                    currentPath ? "?currentPath=" + currentPath : ""
                  }`;
                  anchorElement.download = item.filename;

                  document.body.appendChild(anchorElement);
                  anchorElement.click();

                  document.body.removeChild(anchorElement);
                  // window.URL.revokeObjectURL(href);

                  // alert(`Downloading ${item.filename}`);
                  // axios.get(`${'http://localhost:4000/files'}/download/${item.filename}`, {
                  //   responseType: 'blob',
                  // })
                  // .then(response => {
                  //   const href = window.URL.createObjectURL(response.data);

                  //   const anchorElement = document.createElement('a');

                  //   anchorElement.href = href;
                  //   anchorElement.download = item.filename;

                  //   document.body.appendChild(anchorElement);
                  //   anchorElement.click();

                  //   document.body.removeChild(anchorElement);
                  //   window.URL.revokeObjectURL(href);
                  // })
                  // .catch(error => {
                  //   console.log('error: ', error);
                  // });
                }
              }}
              style={{
                cursor: "pointer",
                backgroundColor: item.isDirectory ? "yellow" : "lightgrey",
                color: "darkblue",
                margin: "4px 4px",
              }}
            >
              <button
                onClick={() => {
                  deleteFile(item.filename);
                }}
              >
                x
              </button>
              {item.filename}
            </li>
          )
        )}
      </ul>
    </>
  );
}

export default App;
